#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
#
#  markup.py
#
#  Copyright 2021  <snoopy@debian>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

"""
Creates a markup to render scribus types

class MARKUP
	to cach markup options
class Fanzine(MARKUP)
	to determine scribus composition

TODO
class Book,Newsletter...(MARKUP)

////////////////////
/ On your own file /
////////////////////

from markup import Fanzine

# get 'scribus00.txt' markupfile, save  'scribus00.sla'
filename = "scribus00"

newDoc(...
openDoc(...

if not objectExists('
	...
else:
	create...(

def markupFunc(*args):
	'''Func to call on markup

"func" arg as string in add_markup
'''
	self = args[0]
	if "make" in args:
		'''close mark, last call'''
		if args[1]:
			insideBox = args[1]
		return
	elif len(args) == 2:
		'''first call'''
		createObject(t,l,w,h,args[1])
	else:
		self.this = args[1]
		key,value = args[2].split(":")
		'''
		on your own filename.txt markupfile:

			# outsideBox
			]
			&this
			key:value
			%
			[box
			 ...
			# insideBox
			[box
			{style
			Lorem ipsum
			 & scape markup
			}
			&this
			key:value
			%
			]

		'''

	return

# first setattr func
setattr(Fanzine,"markupFunc",markupFunc)
# init instance
app = Fanzine()
# app.threads = {} dict to catch all boxes that need all and more page space in
#	self.run(filename). Contains matched *boxes.
#	When a textBox overflows create a new text box and link it
#	Can threading images and text and create infinite threads whith
#	add * before name, *name != **name will create two threads
# [*name] threaded box
# [name] no threaded box

# add_markup
# ¡¡ IMPORTANT !!
# kwargs:
#	name	str required
#				would be self.name after setattr(self,"name") in __init__()
#				you can put 'name' as key in markup kwarg
#	markup	dict required
#				{"markup":"&%"} % -> mark to last call
#				first char must be unique self.marks to see
#				% close last markup open
#				and no " " to scape " &" as no markup
#	func	str required or override "self.run()"
#				func to call on markup must be a string like
#				"self.markupFunc"
#
#
app.add_markup(name="type",markup={"markup":"&%",},func="self.markupFunc")
# ~ print(app.type)
app.run(filename)
# ~ print(dir(app))
# ~ print(getAllStyles())
saveDocAs("{}.sla".format(filename))
"""

__version__ = "0.0.1"

from scribus import *
import re
from copy import copy

fonts = {}

m = 40
marges_p = (m,m,m,m) # Marges de plana (en fanzines A4 2 pagines) scribus.newDocument
marges_d = lambda m:(m+self.size[0]/2.,m,self.size[0]/2.-(2*m),self.size[1]-(2*m)) # Marges de pagina dreta scribus.createText
marges_e = lambda m:(m,m,self.size[0]/2.-(2*m),self.size[1]-(2*m)) # Marges de pagina esquerra scribus.createText

class MARKUP:
	"""Top level class to markups match"""

	fonts = {"markup":"@%","name":"fonts","func":'self.setterFonts'}
	styles = {"markup":"=%","name":"styles","func":'self.setterStyles'}
	box = {"markup":"[]","name":"box","func":'self.setBox'}
	paragraph = {"markup":"{}","name":"paragraph","func":'self.setPar'}
	newline = {"markup":"$","name":"newline","func":"self.addBlankLine"}
	character = {"markup":"()","name":"character","func":None} # re function
	comment = {"markup":"#","name":"comment","func":None} # to no repeat markups
	special = {"markup":"*","name":"special","func":None} # to no repeat markups
	marks = set()
	o_marks = set()
	c_marks = set()
	__all__ = [
		"box",
		"paragraph",
		# ~ "character", not in marks because are re funtion to match it
		"comment",
		'fonts',
		"styles",
		"newline",
		'special',
		]

# ~ @char_name%
	def setterFonts(self,*args):
		if "make" in args:
			return
		elif len(args) == 1:
			fonts[args[0]] = {}
		else:
			key,value = args[1].split(":")
			fonts[args[0]][key] = value

# ~ =style_name%
	def setterStyles(self,*args):
		if "make" in args:
			eval("createParagraphStyle({})".format(",".join(self.this)))
			del self.name
			return
		elif len(args) == 1:
			self.this = []
			self.name = args[0]
		# ~ elif args[1].startswith("parent"):
		else:
			self.this.append(args[1])

# ~ {style_name}
	def setPar(self,*args):
		# ~ print('setPar',args)
		box = getSelectedObject(0)
		# ~ print(box)
		if 'make' in args:
			return
		elif len(args) == 1:
			return
		elif len(args) == 2:
			pass
		else:
			raise Exception("No hauriem d'arribar aqui. No hauria de tenir mes de dos arguments")
		style = args[0]
		t = args[1]
		s = getTextLength(box)
		post_txt = copy(t)
		setStyle(style,box)
		re_char = re.compile(r"^(?P<pre>[^(]*)??[(](?P<key>.*?);(?P<value>.*?)[)](?P<post>.*)")
		chars = re.findall(r"[(](.*?)??;(.*?)??[)]",t,re.MULTILINE)
		if chars:
			while chars:
				re_dict = re.match(re_char,post_txt).groupdict()
				# ~ print(re_dict)
				if re_dict.get("pre"):
					pre_txt = re_dict["pre"]
					insertText(pre_txt,getTextLength(box),box)
					size = False
				else:
					size = True
				char_style = re_dict["key"]
				txt_styled = re_dict["value"]
				post_txt = re_dict["post"]
				s = getTextLength(box)
				insertText(txt_styled,getTextLength(box),box)
				selectText(s,getTextLength(box)-s,box)
				setCharacterStyle(char_style,box,size=size)
				chars.pop()
				deselectAll()
			if post_txt:
				insertText("{}\n".format(post_txt),getTextLength(box),box)
			else:
				insertText("\n",getTextLength(box),box)
		else:
			insertText("{}\n".format(t),s,box)
		deselectAll()
		selectObject(box)

# ~ [name] [*name] [*name] [**name]
	def setBox(self,*args):
		if "make" in args:
			return
		deselectAll()
		if not objectExists(args[0]):
			newPage(-1)
			gotoPage(pageCount())
			box = self.firstTxtBoxFil(args[0])
			deselectAll()
		else:
			box = args[0]
		selectObject(box)

# ~ $
	def addBlankLine(self,box):
		insertText("\n",getTextLength(box),box)

	def add_markup(self,name=None,markup=dict,func=None):
		if name in dir(self):
			raise Exception("Ja hi ha un 'markup' amb aquest nom")
		if markup and not markup.get("name","") and not name:
			raise Exception("S'ha de posar un nom de funcio al markup")
		if markup and not markup.get("markup",""):
			raise Exception("'markup' ha de contenir la clau 'markup' en el diccionari o be es buida")
		if markup["markup"][0] in self.marks:
			raise Exception("Aquesta marca ja està en ús")
		if markup["markup"][0] == "(":
			raise Exception("Aquesta marca ja està en ús")
		if func and not getattr(self,func.split(".")[1]):
			raise Exception("La funcio ha de ser un atribut de la instancia {}".format(str(self)))
		if name:
			markup["name"] = name
		else:
			name = markup["name"]
		if func:
			markup['func'] = func
		exec("self.{} = {}".format(name,markup))
		self.__all__.append(name)
		self.__init__()

	def __run__(self,filename):
		with open("../txts/{}.py".format(filename),"r") as t_file:
			text = t_file.read()
		while "\n\n" in text:
			text = text.replace("\n\n","\n")
		while "\t" in text:
			text = text.replace("\t","")
		opens = []
		name = ""
		c = None
		txt_l = text.split("\n")
		for n,txt in enumerate(txt_l):
			if not txt or txt[0]=="#":
				continue
			if txt[0] in self.o_marks:
				_name = "{:0>3}_{}".format(n,self.marks[txt[0]])
				name = txt[1:]
				if self.marks[txt[0]] in ("box"):
					c = name
				if eval('self.{}["func"]'.format(self.marks[txt[0]])):
					eval("{}('{}')".format(eval('self.{}["func"]'.format(self.marks[txt[0]])),txt[1:]))
					exec("func = {}".format(eval('self.{}["func"]'.format(self.marks[txt[0]]))))
				opens.append(_name)
			elif txt[0] in self.c_marks:
				j = opens.pop()
				if txt[0] == "]" and "*" in c:
					deselectAll()
					selectObject(c)
					self.createFilText(c)
					c = None
				elif txt[0] == "%":
					deselectAll()
					if c:
						selectObject(c)
					elif "imatge" in j:
						func(c,"make")
						self.createFilText(name)
			elif txt[0] in self.marks:
				eval("{}(c)".format(eval('self.{}["func"]'.format(self.marks[txt[0]]))))
			else:
				func(name,txt)

class Fanzine(MARKUP):
	"""Class to get scribus definitions as Doc, size...."""

	def __init__(self):
		marks = []
		o_marks = []
		c_marks = []
		for f in self.__all__:
			if len(getattr(self,f)["markup"]) > 1:
				o_marks.append((getattr(self,f)["markup"][0],getattr(self,f)["name"]))
				c_marks.append((getattr(self,f)["markup"][1],getattr(self,f)["name"]))
			for mrk in eval("self.{}['markup']".format(f)):
				marks.append((mrk,eval("self.{}['name']".format(f))))
		self.marks = dict(set(marks))
		self.o_marks = dict(set(o_marks))
		self.c_marks = dict(set(c_marks))
		self.threads = {}

	def _run(self,filename):
		self.margins = (m,m,m,m)
		self.defaultPos = (m,m)
		self.firstPos = (461,160)
		self.size = getPageSize()
		self.marges_p = (m,m,m,m) # Marges de plana (en fanzines A4 2 pagines) scribus.newDocument
		self.marges_d = lambda m:(m+self.size[0]/2.,m,self.size[0]/2.-(2*m),self.size[1]-(2*m)) # Marges de pagina dreta scribus.createText
		self.marges_e = lambda m:(m,m,self.size[0]/2.-(2*m),self.size[1]-(2*m)) # Marges de pagina esquerra scribus.createText
		self.__run__(filename)
		for t in sorted(self.threads):
			self.posThreadBoxes(self.threads[t])

	def createFilText(self,box):
		if box in self.threads:
			del self.threads[box]
		master_key = "*"*box.count("*")
		fil_name = "{:0>2}.fil.{}".format(len(self.threads),box)
		box_n = "{:0>2}.{}"
		if self.threads.get(master_key,""):
			self.threads[master_key].update({fil_name:[box_n.format(0,box)]})
		else:
			self.threads[master_key] = {fil_name:[box_n.format(0,box)]}
		a,b,c,d = self.marges_e(m)
		if objectExists(box):
			setNewName(self.threads[master_key][fil_name][-1],box)
		else:
			newPage(-1)
			createText(a,b,c,d,self.threads[master_key][fil_name][-1])
			gotoPage(pageCount())
		if getObjectType(self.threads[master_key][fil_name][-1]) != "TextFrame":
			return
		try:
			while textOverflows(self.threads[master_key][fil_name][-1]):
				box_name = box_n.format(len(self.threads[master_key][fil_name]),box)
				a = m+10*len(self.threads[master_key][fil_name])
				self.threads[master_key][fil_name].append(createText(a,a,c,d,box_name))
				linkTextFrames(self.threads[master_key][fil_name][-2],self.threads[master_key][fil_name][-1])
		except:
			pass

	def posThreadBoxes(self,th):
		boxes = [box for fil in sorted(th) for box in th[fil]]
		print(boxes)
		if boxes[0].count("*") == 1:
			i = 3
		else:
			newPage(-1)
			i = pageCount()
		while len(boxes)%4 != 0:
			boxes.append("*")
		fil_pages = len(boxes)/2
		while pageCount() - i - fil_pages + 1 < 0:
			newPage(-1)
		for n in range(fil_pages):
			gotoPage(i+n)
			if n%2:
				if getSize(boxes[n])[1] == self.size[1]-(5*m):
					moveObjectAbs(self.firstPos[0],self.firstPos[1],boxes[n])
				else:
					moveObjectAbs(m,m,boxes[n])
				if boxes[-n-1] != "*":
					if getSize(boxes[-n-1])[1] == self.size[1]-(5*m):
						moveObjectAbs(self.firstPos[0],self.firstPos[1],boxes[-n-1])
					else:
						moveObjectAbs(m+self.size[0]/2.,m,boxes[-n-1])
			else:
				if getSize(boxes[n])[1] == self.size[1]-(5*m):
					moveObjectAbs(self.firstPos[0],self.firstPos[1],boxes[n])
				else:
					moveObjectAbs(m+self.size[0]/2.,m,boxes[n])
				if boxes[-n-1] != "*":
					if getSize(boxes[-n-1])[1] == self.size[1]-(5*m):
						moveObjectAbs(self.firstPos[0],self.firstPos[1],boxes[-n-1])
					else:
						moveObjectAbs(m,m,boxes[-n-1])

	def firstTxtBoxFil(self,box):
		return createText(self.firstPos[0],self.firstPos[1],((self.size[0]-(2*m))/2.)-m, self.size[1]-(5*m),box)

	# ~ def defaultTxtBoxFil(self,box):

	# ~ def __str__(self):
		# ~ return "Fanzine"

def setCharacterStyle(style,box,size=False):
	setFont(fonts[style]["font"],box)
	if size:
		setFontSize(float(fonts[style]["size"]),box)
